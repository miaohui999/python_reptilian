#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Created on 2020-03-08 21:45:02
# Project: CSGO

from pyspider.libs.base_handler import *


class Handler(BaseHandler):
    crawl_config = {
    }

    @every(minutes=24 * 60)
    def on_start(self):
        self.crawl('https://www.stmbuy.com/csgo', callback=self.index_page,validate_cert=False)

    @config(age=10 * 24 * 60 * 60)
    def index_page(self, response):
        for each in response.doc('.goods-list a').items():
            self.crawl(each.attr.href, callback=self.detail_page,validate_cert=False)
        next=response.doc('.page-next > a').attr.href
        self.crawl(next, callback=self.index_page,validate_cert=False)
        
        

    @config(priority=2)
    def detail_page(self, response):
        return {
            "url": response.url,
            '武器名称':response.doc('.goods-name').text(),
            '最近成交价':response.doc('.goods-price > strong').text(),
            '当前在售':response.doc('.now-count strong').text(),
            '累计出售':response.doc('.total-sell > strong').text(),
            
            
            
        }