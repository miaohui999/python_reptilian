import requests
from pyquery import PyQuery as pq


#查询火车票函数
def Train_tickets():
    Place_of_departure = input('请输入出发的地方:')
    Destination = input('请输入目的地:')
    Gotime = input('请输入出发的时间:')
    #查询的车票的接口地址
    url='http://www.chepiao100.com/yupiao/{},{},{}.html'.format(Place_of_departure,Destination,Gotime)

    request=requests.get(url).text


    #解析出我们想要的信息
    doc=pq(request)
    item=doc('.items li').items()

    for i in item:
        #动车组号
        #print(i.find('.code_wp_1 ').text())
        #出发时间
        #print(i.find('.time_wp .start').text())
        #到达时间
        #print(i.find('.time_wp .arrive').text())

        data={
            '动车号':i.find('.code_wp_1').text(),
            '出发时间':i.find('.time_wp_t').text(),
            '出发时间':i.find('.time_wp .start').text(),
            '到达时间':i.find('.time_wp .arrive').text(),
            '运行时间':i.find('.time_wp_t').text(),
            '票价':i.find('.pri_wp').text(),
            '票数':i.find('.org').text()

        }
        print(data)

#查询汽车票函数
def Bus_ticket():
    c=input('请输入出发的城市:')
    d=input('请输入目的地:')
    #汽车票查询
    url='http://www.chepiao100.com/qichepiao/{},{}.html'.format(c,d)
    request=requests.get(url).text
    doc=pq(request)
    Train_number=doc.find('.info').text()
    item=doc('.items li').items()
    print(Train_number)
    for i in item:

        data={
            '出发站':i.find('.ca_start').text(),
            '目的地':i.find('.sl_arrive').text(),
            '发车时间':i.find('.ca_stime').text(),
            '票价':i.find('.ca_price').text(),
            '车型':i.find('.ca_grade').text()
        }

        print(data)

#查询火车票代售点函数
def Train_sale():
    print('输入您的地点 格式如:广西 柳州 城中区')
    a=input('请输入省份:')
    b=input('请输入城市名:')
    c=input('请输入区名:')
    #爬取火车代售点
    url='http://www.chepiao100.com/daishoudian/{},{},{}.html'.format(a,b,c)
    request=requests.get(url).text
    doc=pq(request)
    Selling_point=doc('.info').text()
    item=doc('.items li').items()
    print(Selling_point)
    for i in item:

        data={
            '代售点名称':i.find('.sl_na').text(),
            '地址':i.find('.sl_ad').text(),
            '营业时间':i.find('.sl_tm').text(),
            '联系电话':i.find('.sl_ph').text()
        }
        print(data)

#查询车站
def Station_enquiry():

    #查询车站(查询车站日期中进站的所有动车 =。=运煤运货的可能查不到)
    print('查询车站名的格式:如 南宁东 南宁')
    print('时间格式 xxxx-xx-xx 2020-02-09')
    a = input('请输入车站名:')
    b = input('请输入查询的时间:')
    url='http://www.chepiao100.com/chezhan/{},{}.html'.format(a,b)

    request=requests.get(url).text
    doc=pq(request)
    Train_number=doc('.info').text()
    print(Train_number)
    item=doc('.items li').items()
    for i in item:

        data={
            '列车时刻': i.find('.rem_wp_sta').text(),
            '动车组号':i.find('.code_wp_1 a').text(),
            '发车时间':i.find('.time_wp .start').text(),
            '收车时间':i.find('.time_wp .arrive').text(),
            '运行时间':i.find('.time_wp_t').text(),
            '票价':i.find('.pri_wp_sta').text()


        }
        print(data)

#车次查询
def Train_number_inquiry():
    print('车次格式 如:D1 开头字母需大写')
    print('时间格式 xxxx-xx-xx 2020-01-01')
    a=input('请输入列车组号:')
    b=input('请输入查询的日期:')
    url='http://www.chepiao100.com/checi/{},{}.html'.format(a,b)
    request=requests.get(url).text
    doc=pq(request)
    item=doc('.items').items()
    for i in item:


        data={
            '动车号':i.find('.code_wp_1 a').text(),
            '发车时间':i.find('.time_wp .arrive').text(),
            '到达时间':i.find('.time_wp .start').text(),
            '运行时间':i.find('.time_wp_t').text(),
            '列车时刻':i.find('.org').text()
        }
        print(data)


print('*'*30)
print('欢迎使用列车 汽车余票查询系统')
print('输入对应的序号 查询对应的功能')
print('     作者:一个耿直的小爬虫')
print('     2020年1月27日          ')
print('*'*30)


print('-'*30)
print('1.查询火车票')
print('2.查询汽车票')
print('3.查询火车代售点')
print('4.查询车站')
print('5.查询车次')
print('-'*30)

a=input('请输入序号:')
if a =='1':
    Train_tickets()
elif a == '2':
    Bus_ticket()
elif a == '3':
    Train_sale()
elif a == '4':
    Station_enquiry()
elif a == '5':
    Train_number_inquiry()